# -*- coding: utf-8 -*-
import scrapy
from scrapy_splash import SplashRequest
from scrapy_splash import SplashMiddleware
import time

now_page = 1


class UnSpider(scrapy.Spider):
    name = 'un'

    def __init__(self):
        self.pagelist = []
        self.url = "### url"

    def start_requests(self):
        yield SplashRequest(url=self.url,callback=self.parse)

    def parse(self, response):
        print("node1")
        global now_page
        global script
        link = response.xpath('.//table[@class="sch-grid-standard"]/tbody/tr/td[not(@*)]/a[1]/@onclick').extract()
        title = response.xpath('.//table[@class="sch-grid-standard"]/tbody/tr/td[not(@*)]/a[1]/text()').extract()
        location = response.xpath('.//table[@class="sch-grid-standard"]/tbody/tr/td[@align="left"][6]/text()').extract()
        pages = response.xpath('.//tr[@class="pager"]/td/table/tbody/tr/td/a/text()').extract()
        #pages = response.xpath('')
        for l, t in zip(link, title) :
            #items
            print("link: "+ l.split("'")[1])
            print("title: "+ t)
            time.sleep(2)
        print("*******"*20)
        for i, v in enumerate(pages):
            v = self.to_int(v)
            if type(v) == int:
                if v - now_page == 1:
                    print(f"index {i}")
                    print(f"value {v}")
                    script = f"""
                    function main(splash, args)
                      splash.images_enabled = false
                      assert(splash:go(args.url))
                      assert(splash:wait(1))
                      js = string.format("document.querySelector('tr.pager>td>table>tbody>tr').querySelectorAll('td')[{i}].querySelector('a').click()", args.page)
                      splash:runjs(js)
                      assert(splash:wait(2))
                      return splash:html()
                    end
                    """
                    print("*******"*20)
                    yield SplashRequest(url=self.url, callback=self.parse, dont_filter=True,endpoint='execute', args={
                                    'wait': 10, 'images': 0, 'lua_source': script})
                    now_page = v


    def to_int(self,ele):
        try:
            return int(ele)
        except:
            return str(ele)
